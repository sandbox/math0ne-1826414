A drupal module and some associated elisp code that will allow you to manage
drupal content in emacs org-mode, be aware that if you use this module to manage
a content type, you cannot make any changes to that content through drupal.

FEATURES

* Manage a drupal content type with a directory of org files
* CCK Support
* Taxonomy Support
* Push based publishing (your site updates in real time)
* Node image Support
* URL Alias support
* Uses org generated html for body of post

TODO

* Allow editing of org files in drupal
* Pull changed org files into emacs
* More secure push based publishing (api module?)
* Create org specifice css
